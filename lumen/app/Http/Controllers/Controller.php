<?php namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    public function contact(Request $request)
    {
        $fields = $request->all();
        Mail::send('emails.message', $fields, function($message) {
            $message->to('bynoil@gmail.com', 'Bobko Maria')->subject('Сообщение от bobkomaria.by');
        });

        return json_encode(['status' => 'send']);
    }
}
