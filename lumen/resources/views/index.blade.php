<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"  dir="ltr" lang="en-US"> <!--<![endif]-->

<head>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Maria Bobko personal website">

    <title>Maria Bobko</title>
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel='stylesheet' id='reset-css'  href='css/reset.css' type='text/css' media='all' />
    <link rel='stylesheet' id='style-css'  href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='styles-css'  href='css/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' id='flexslider-css'  href='css/flexslider.css' type='text/css' media='all' />
    <link rel='stylesheet' id='fancybox-css'  href='css/jquery.fancybox.css' type='text/css' media='all' />
    <link rel='stylesheet' id='font-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C300italic%2C700%2C600%2C800' type='text/css' media='all' />
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/modernizr.js'></script>
    {{--<script type='text/javascript' src='js/comment-reply.dev.js'></script>--}}
    <link rel='canonical' href='/' />
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <style type="text/css" id="custom-background-css">
        body.custom-background { background-color: #000; background-image: url('images/bg-html.png'); background-repeat: repeat; background-position: top left; background-attachment: fixed; }
    </style>
    <!--[if gt IE 8]><!--><link href="css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /><!--<![endif]-->
    <!--[if !IE]> <link href="css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
    <!--[if lt IE 9]> <link href="css/styles-ie8.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
</head>

<body class="home page page-id-5 page-template page-template-page-page-builder-php custom-background">

<!-- Preloader -->
<div id="preloader">
    <div id="status">
        <div class="windows8">
            <div class="wBall" id="wBall_1">
                <div class="wInnerBall">
                </div>
            </div>
            <div class="wBall" id="wBall_2">
                <div class="wInnerBall">
                </div>
            </div>
            <div class="wBall" id="wBall_3">
                <div class="wInnerBall">
                </div>
            </div>
            <div class="wBall" id="wBall_4">
                <div class="wInnerBall">
                </div>
            </div>
            <div class="wBall" id="wBall_5">
                <div class="wInnerBall">
                </div>
            </div>
        </div>
    </div>
</div>
<header id="wrapper">
    <div class="container clearfix">
        <h1 id="logo">
            <a href="#filter=.home"><img src="images/bg-logo-2.png" alt="logo" /></a>
        </h1>
        <!-- start navi -->
        <div id="options" class="clearfix">
            <ul id="filters" class="option-set clearfix"><li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"><a href="#filter=.home">Главная</a></li>
                <li id="menu-item-17" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17"><a href="#filter=.services">Услуги</a></li>
                <li id="menu-item-19" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-19"><a href="#filter=.portfolio">Портфолио</a></li>
                <li id="menu-item-20" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-20"><a href="#filter=.about">Обо мне</a></li>
                <li id="menu-item-22" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-22"><a href="#filter=.contact">Контакты</a></li>
            </ul>    </div>
        <div id="nav-button-wrapper">
            <div class="nav-button"><a href="#" onClick="return false"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </a></div>
        </div>
        <!-- end nav -->
    </div>
</header>
<!-- end header -->
<div class="container">
    <div id="container" class="clearfix">
        <div class="element clearfix col1-3 home middle about">
            <div class="images">
                <a href="images/mb_big_photo.jpg" rel="group">
                    <img src="images/mb_photo.jpg" />
                </a>
            </div>
        </div>
        <a href="#filter=.about">
            <div class="element clearfix col2-3 home">
                <div class="block-bg">
                    <div class="number"><div class="nav-button"><span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span></div></div>
                    <h3 style="padding-bottom: 100px"><span>"Архитектура и строительство - разные понятия. Нам выбирать строить или творить".&copy;</span></h3>
                </div>
            </div>
        </a>
        <div class="element home clearfix col1-3 middle">
            <div class="certificate">
                <img src="images/bg-certificate.png" alt="icon" />
            </div>
            <div class="barrier"></div>
            <p><strong>Свидетельство</strong> о государственной регистрации.<br /><span>УНП 192501600</span><br /><a href="images/shutterstock_79892062.jpg" target="_blank">Посмотреть (PDF)</a></p>
        </div>
        <a href="#filter=.services">
            <div class="element clearfix col2-3 home">
                <div class="block-bg">
                    <div class="number">
                        <div class="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
                    </div>
                    <h2>Услуги</h2>
                    <h3>Проектирование зданий и сооружений 5 класса сложности, индивидуальных жилых домов, авторский надзор, консультации, итд.</h3>
                </div>
            </div>
        </a>
        <div class="element home clearfix col2-3 map contact">
            <div id="map"></div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                setTimeout(function () {
                    // initiate googlemaps
                    jQuery("#map").goMap({ address: "yanki mavra 50 minsk, belarus",
                        zoom: 14,
                        navigationControl: true,
                        maptype: "ROADMAP",
                        draggable: false, zoomControl: false, scrollwheel: false, disableDragging: true,
                        markers: [
                            { "address" : "yanki mavra 50 minsk, belarus" }
                        ],
                        icon: "images/custom-marker.png",
                        addMarker: true,
                        disableDoubleClickZoom: true
                    });

                    var styles = [ { "stylers": [ { "visibility": "on" }, { "saturation": -100 }, { "gamma": 1 } ] },{ } ];

                    $.goMap.setMap({
                        mapTypeControlOptions: {
                            mapTypeIds: [google.maps.MapTypeId.ROADMAP, "Мария Бобко"]
                        }});

                    var styledMapOptions = {
                        name: "Мария Бобко"
                    }

                    var jayzMapType = new google.maps.StyledMapType(
                            styles, styledMapOptions);

                    $.goMap.map.mapTypes.set("Мария Бобко", jayzMapType);
                    $.goMap.map.setMapTypeId("Мария Бобко");



                    var styleArray = [{
                        featureType: "poi.business",
                        elementType: "labels",
                        stylers: [{ visibility: "off" }]
                    }];
                    $.goMap.setMap({styles: styleArray});
                }, 100);
            });
        </script>

        <div class="element clearfix col1-3 home contact no-padding">
            <div id="contact">
                <div id="message"></div>
                <form method="post" action="/contact" name="contactform" id="contactform" autocomplete="off">
                    <fieldset>
                        <div class="alignleft padding-right">
                            <label for="name" accesskey="U"><span class="required">Name</span></label>
                            <input name="name" type="text" id="name" size="30" title="Name *" />
                            <label for="email" accesskey="E"><span class="required">Email</span></label>
                            <input name="email" type="text" id="email" size="30" title="Email *" />
                            <label for="phone" accesskey="P"><span class="required">Phone</span></label>
                            <input name="phone" type="text" id="phone" size="30" title="Phone *" class="third" />
                        </div>
                        <label for="comments" accesskey="C"><span class="required">Comments</span></label>
                        <textarea name="comments" cols="40" rows="3" id="comments" title="Comment *"></textarea>
                        <input type="submit" class="submit" id="submit" value="Submit" />
                        <input type="text" id="check" name="check">
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="element clearfix col1-3 home contact">
            <h2>ИП Мария Бобко</h2><p>Янки Мавра 50, кв. 41<br />
                Минск, 220015<br />
                m.s.bobko@gmail.com<br />
                (МТС) +375 (29) 706-49-45</p>
        </div>


        <div class="element clearfix col1-3 portfolio23 overflow  load_content portfolio">
            <div class="movable-content">
                <div class="images">
                    <img src="images/Unknown-300x216.jpeg" alt="Some Project title" />
                </div>
                <div class="title">
                    <h4>Some Project title</h4>
                    <p>Webdesign</p>
                </div>
            </div>
            <div class="infos">
                <div class="nav-button2 center"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
                <a href="#filter=.portfolio%3Anot(.portfolio),+.portfolio23load" class="tippy">
                    <div class="tab"><i class="icons view-more"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View Info</div>
                    </div>
                </a>
                <div class="clear"></div>

                <a href="images/Unknown.jpeg" class="popup tippy">
                    <div class="tab"><i class="icons zoom"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">Open in Lightbox</div>
                    </div>
                </a>
                <div class="clear"></div>

                <a href="images/Unknown.jpeg" class="tippy">
                    <div class="tab"><i class="icons pdf"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">Link to PDF File</div>
                    </div>
                </a>
                <div class="clear"></div>

                <a href="http://www.google.com" target="_blank" class="tippy">
                    <div class="tab last"><i class="icons link"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View Website</div>
                    </div>
                </a>
                <div class="clear"></div>

                <div class="hover-content">
                    <span>Client: Microsoft<br />Date: November 2012<br />HTML5 / CSS3<br />Retina-Ready<br /></span>
                </div>
            </div>
        </div>

        <span id="23" class="load-wrap">
            <div class="element clearfix portfolio23load col1-1 auto no-border">
                <div class="flexslider">
                    <div class="images">
                        <ul class="slides">
                            <li><img src="images/shutterstock_84787564.jpg" alt="slide"/></li>
                            <li><img src="images/shutterstock_79892062.jpg" alt="slide"/></li>
                        </ul>
                        <a href="#filter=.portfolio">
                            <div class="icon close"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="element portfolio23load  clearfix col1-3 " id="aq_block_1">
                <h2>Project Details</h2>
                <ul class="list check">
                    <li>Client: Microsoft</li>
                    <li>Date: November 2012</li>
                    <li>HTML5 / CSS3</li>
                    <li>Retina-Ready</li>
                </ul>
            </div>
            <div class="element portfolio23load clearfix col2-3  ">
                <div class="block-bg">
                    <h2>Project Title Here</h2>
                    <h3>Lorem ipsum dolor sit amet, <span>consectetur adipiscing</span> elit. Vestibulum id ligula felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</h3>
                </div>
            </div>
            <div class="element portfolio23load clearfix col1-3  " id="aq_block_3">
                <p class="blockquote">“ This could be a blockquote alright. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer commodo tristique odio. ” <span><br />JOHN DOE, COMPANY NAME</span></p>
            </div>
        </span>


        <div class="element clearfix col1-3 portfolio898 overflow  load_content portfolio">
            <div class="movable-content">
                <div class="images">
                    <img src="images/shutterstock_84787564-300x216.jpg" alt="Some Project Title" />
                </div>
                <div class="title">
                    <h4>Some Project Title</h4>
                    <p>Webdesign</p>
                </div>
            </div>
            <div class="infos">
                <div class="nav-button2 center"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
                <a href="#filter=.portfolio%3Anot(.portfolio),+.portfolio898load" class="tippy">
                    <div class="tab"><i class="icons view-more"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View More</div>
                    </div>
                </a>
                <div class="clear"></div>

                <a href="images/shutterstock_84787564.jpg" class="popup tippy">
                    <div class="tab"><i class="icons zoom"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View in Lightbox</div>
                    </div>
                </a>
                <div class="clear"></div>

                <a href="http://www.google.com" target="_blank" class="tippy">
                    <div class="tab last"><i class="icons link"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View Website</div>
                    </div>
                </a>
                <div class="clear"></div>

                <div class="hover-content">
                    <span>Client: Microsoft<br />Date: 11/02/2012<br />HTML5/CSS3<br />Retina Ready<br />A Third Item<br /></span>
                </div>
            </div>
        </div>

        <span id="898" class="load-wrap">
            <div class="element clearfix portfolio898load col1-1 auto no-border">
                <div class="images">
                    <img width="940" height="620" src="images/shutterstock_84787564.jpg" class="attachment-post-thumbnail wp-post-image" alt="shutterstock_84787564" />
                    <a href="#filter=.portfolio">
                        <div class="icon close"></div>
                    </a>
                </div>
            </div>
            <div class="col1-1 element clearfix portfolio898load hentry">
                <h2>Some Project Title</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.Nullam quis risus eget urna mollis ornare.</p>
                <p class="blockquote">“ This could be a blockquote. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer commodo tristique odio. ”<br /><span>JOHN DOE, COMPANY NAME</span></p>
                <p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.</p>
            </div>

            <div class="element portfolio898load  clearfix col1-3 " id="aq_block_1">
                <h2>Project Details</h2>
                <ul class="list check">
                    <li>Client: Microsoft</li>
                    <li>Date: November 2012</li>
                    <li>HTML5 / CSS3</li>
                    <li>Retina-Ready</li>
                </ul>
            </div>
            <div class="element portfolio898load clearfix col2-3  ">
                <div class="block-bg">
                    <h2>Project Title Here</h2>
                    <h3>Lorem ipsum dolor sit amet, <span>consectetur adipiscing</span> elit. Vestibulum id ligula felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</h3>
                </div>
            </div>
            <div class="element portfolio898load clearfix col1-3  " id="aq_block_3"><p class="blockquote">“ This could be a blockquote alright. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer commodo tristique odio. ” <span><br />JOHN DOE, COMPANY NAME</span></p></div>
        </span>

        <div class="element clearfix col1-3 portfolio90 overflow  load_content portfolio">
            <div class="movable-content">
                <div class="images">
                    <img src="images/shutterstock_79892062-300x216.jpg" alt="Some Project Title" />
                </div>

                <div class="title">
                    <h4>Some Project Title</h4>
                    <p>Illustration, Photography</p>
                </div>
            </div>

            <div class="infos">
                <div class="nav-button2 center">
                    <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
                <a href="#filter=.portfolio%3Anot(.portfolio),+.portfolio90load" class="tippy">
                    <div class="tab"><i class="icons view-more"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View More</div>
                    </div>
                </a>
                <div class="clear"></div>

                <a href="images/shutterstock_79892062.jpg" class="popup tippy">
                    <div class="tab"><i class="icons zoom"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View in Lightbox</div>
                    </div>
                </a>
                <div class="clear"></div>

                <a href="images/modified.jpg" class="tippy">
                    <div class="tab"><i class="icons pdf"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View .PDF File</div>
                    </div>
                </a>
                <div class="clear"></div>

                <a href="http://www.google.com" target="_blank" class="tippy">
                    <div class="tab last"><i class="icons link"></i></div>
                    <div class="alignleft">
                        <div class="hover-title">View Website</div>
                    </div>
                </a>
                <div class="clear"></div>

                <div class="hover-content">
                    <span>Client: Microsoft<br />Date: 11/02/2012<br />HTML5/CSS3<br />Retina Ready<br />A Third Item<br /></span>
                </div>
            </div>
        </div>

        <span id="90" class="load-wrap">
            <div class="element clearfix portfolio90load col1-1 auto no-border">
                <div class="images">
                    <a class="video-popup" href="http://player.vimeo.com/video/25624940" title="Some Project Title">
                        <img width="940" height="620" src="images/shutterstock_79892062.jpg" class="attachment-post-thumbnail wp-post-image" alt="shutterstock_79892062" />
                    </a>
                    <a href="#filter=.portfolio">
                        <div class="icon close"></div>
                    </a>
                </div>

            </div>
            <div class="col1-1 element clearfix portfolio90load hentry">
                <h2>Some Project Title</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.Nullam quis risus eget urna mollis ornare.</p>
                <p class="blockquote">“ This could be a blockquote. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer commodo tristique odio. ”<br /><span>JOHN DOE, COMPANY NAME</span></p>
                <p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.</p>
            </div>

            <div class="element portfolio90load  clearfix col1-3 " id="aq_block_1">
                <h2>Project Details</h2>
                <ul class="list check">
                    <li>Client: Microsoft</li>
                    <li>Date: November 2012</li>
                    <li>HTML5 / CSS3</li>
                    <li>Retina-Ready</li>
                </ul>
            </div>
            <div class="element portfolio90load clearfix col2-3  ">
                <div class="block-bg">
                    <h2>Project Title Here</h2>
                    <h3>Lorem ipsum dolor sit amet, <span>consectetur adipiscing</span> elit. Vestibulum id ligula felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</h3>
                </div>
            </div>
            <div class="element portfolio90load clearfix col1-3  " id="aq_block_3"><p class="blockquote">“ This could be a blockquote alright. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer commodo tristique odio. ” <span><br />JOHN DOE, COMPANY NAME</span></p></div>
        </span>





        <div class="element clearfix col1-3 portfolio75 overflow  load_content portfolio">

            <div class="movable-content">

                <div class="images">
                    <img src="images/shutterstock_84787564-300x216.jpg" alt="Some Project Title" />
                </div>

                <div class="title">
                    <h4>Some Project Title</h4>		 	              <p>Illustration, Photography</p>
                </div>

            </div>

            <div class="infos">
                <div class="nav-button2 center"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>


                <a href="#filter=.portfolio%3Anot(.portfolio),+.portfolio75load" class="tippy">

                    <div class="tab"><i class="icons view-more"></i></div><div class="alignleft">
                        <div class="hover-title">View More</div>			 		        	</div>
                </a><div class="clear"></div>

                <a href="images/shutterstock_84787564.jpg" class="popup tippy">
                    <div class="tab"><i class="icons zoom"></i></div><div class="alignleft">
                        <div class="hover-title">View in Lightbox</div>			 		        	</div>
                </a><div class="clear"></div>


                <a href="http://www.google.com" target="_blank" class="tippy">
                    <div class="tab last"><i class="icons link"></i></div><div class="alignleft">
                        <div class="hover-title">View Website</div>		 		        	</div>
                </a><div class="clear"></div>

                <div class="hover-content">
		 				        <span>
		 				        	Client: Microsoft<br />		 				        	Date: 11/02/2012<br />		 				        	HTML5/CSS3<br />Retina Ready<br />A Third Item<br />		 				        </span>
                </div>
            </div>

        </div>

		 	  <span id="75" class="load-wrap">
		 	    <div class="element clearfix portfolio75load col1-1 auto no-border">



                    <div class="images">

                        <img width="940" height="620" src="images/shutterstock_84787564.jpg" class="attachment-post-thumbnail wp-post-image" alt="shutterstock_84787564" />
                        <a href="#filter=.portfolio">
                            <div class="icon close"></div>
                        </a>

                    </div>


                </div>

		 	    		 	    <div class="col1-1 element clearfix portfolio75load hentry">
                                    <h2>Some Project Title</h2>		 	    		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.Nullam quis risus eget urna mollis ornare.</p>
                                    <p class="blockquote">“ This could be a blockquote. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer commodo tristique odio. ”<br /><span>JOHN DOE, COMPANY NAME</span></p>
                                    <p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.</p>
                                </div>

		 	    <div class="element portfolio75load  clearfix col1-3 " id="aq_block_1"><h2>Project Details</h2><ul class="list check"><li>Client: Microsoft</li><li>Date: November 2012</li><li>HTML5 / CSS3</li><li>Retina-Ready</li></ul></div><div class="element portfolio75load clearfix col2-3  ">
                      <div class="block-bg"><h2>Project Title Here</h2><h3>Lorem ipsum dolor sit amet, <span>consectetur adipiscing</span> elit. Vestibulum id ligula felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</h3>
                      </div>
                  </div><div class="element portfolio75load clearfix col1-3  " id="aq_block_3"><p class="blockquote">“ This could be a blockquote alright. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer commodo tristique odio. ” <span><br />JOHN DOE, COMPANY NAME</span></p></div>
		 	 </span>








        <div class="element clearfix col1-1 load-content-blog about">
            <h2 class="blog-heading">Меня зовут Мария и я архитектор.</h2>
            <div class="the-content hentry">
                <p>В 2012 году окончила Белорусский Национальный технический университет. Несколько лет работала в компании «Терем-Теремок», под началом Кононовича С.А. С июля 2015 года занимаюсь частной практикой.</p>
                <p>Моя работа заключается в том, что бы помочь клиентам в создании дома, отражающего жизнь людей, которые там живут.</p>
            </div>
            <div class="break"></div>
        </div>

        <div class="element clearfix col1-3 middle about">
            <div class="certificate">
                <img src="images/bg-certificate.png" alt="icon" />
            </div>
            <div class="barrier"></div>
            <p><strong>Свидетельство</strong> о государственной регистрации.<br /><span>УНП 192501600</span><br /><a href="images/shutterstock_79892062.jpg" target="_blank">Посмотреть (PDF)</a></p>
        </div>

        <div class="element clearfix col1-1 load-content-blog services">
            <h2 class="blog-heading">Услуги</h2>
            <ul>
                <li><p>Проектирование зданий и сооружений 5 класса сложности;</p></li>
                <li><p>Проектирование индивидуальных жилых домов;</p></li>
                <li><p>Проектирование придомовых построек;</p></li>
                <li><p>Проектирование малых архитектурных форм;</p></li>
                <li><p>Разработка концепций ландшафтной организации придомовой территории;</p></li>
                <li><p>Авторский надзор;</p></li>
                <li><p>Консультации.</p></li>
            </ul>
            <div class="break"></div>
        </div>
        <div class="element clearfix col1-3  contact">
            <h2>Время работы:</h2>
            <p>С понедельника по пятницу:<br />
                09:00 - 18:00<br />
                С субботы по воскресенье:<br />
                по предварительной записи.</p>
        </div>
    </div>
</div>

<footer class="centered">

    <div class="container">
        <div class="clear"></div>
    </div>

    © 2015, Maria Bobko.
    <ul class="social clearfix">
    </ul>
</footer>
<!-- end container -->

<!-- BACK TO TOP BUTTON -->
<div id="backtotop">
    <ul>
        <li><a id="toTop" href="#" onClick="return false"></a></li>
    </ul>
</div>

<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false'></script>
<script type='text/javascript' src='js/jquery-easing-1.3.js'></script>
<script type='text/javascript' src='js/custom.js'></script>
<script type='text/javascript' src='js/jquery.gomap-1.3.2.min.js'></script>
<script type='text/javascript' src='js/jquery.isotope.min.js'></script>
<script type='text/javascript' src='js/jquery.ba-bbq.min.js'></script>
<script type='text/javascript' src='js/jquery.isotope.load_home.js'></script>
<script type='text/javascript' src='js/jquery.form.js'></script>
<script type='text/javascript' src='js/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='js/input.fields.js'></script>
<script type='text/javascript' src='js/responsive-nav.js'></script>
<script type='text/javascript' src='js/scrollup.js'></script>
<script type='text/javascript' src='js/preloader.js'></script>
<script type='text/javascript' src='js/jquery.fancybox.pack.js'></script>
</body>
</html>